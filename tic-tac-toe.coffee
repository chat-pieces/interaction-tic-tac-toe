fs = require 'fs'
timeout = (secs, callback)-> setTimeout callback, secs*1000
interval = (secs, callback)-> setInterval callback, secs*1000

cmdRE = null

module.exports = ticTacToe = (bot, update)->
    if update._kind is 'callback_query'
        if match = update._act?.data.match /^tic-tac-toe:(.)x(.)/
            return playerAction bot, update._act, parseInt(match[1]), parseInt(match[2])
    msg = update._act?.text or ''
    chatId = update._act?.chat?.id or 0
    msgId = update._act?.message_id or 0
    usr = update._act?.from.username
    gName = '(?:velha|tic[-_\\s]?tac[-_\\s]?toe)'
    cmdRE ?= ///^\s*(?:
    (?:/#{gName}\s|/#{gName}@#{bot.username}\s)(?:\s*@([^\s]+))?(?:[\s.,;:!?].*)?
    |
    /#{gName}(?:@#{bot.username})?\s*
    )$///i
    cmd = msg.match cmdRE
    if cmd
        if cmd[1]
            initGame usr, cmd[1], chatId, bot
        else
            bot.sendMessage 'Use "/velha @alguem",
            então iniciarei um jogo para você e esse alguém.', chatId, reply_to_message_id: msgId

memoFile = (chatId, msgId)-> "/tmp/tic-tac-toe-#{chatId}-#{msgId}.json"
readMemo = (chatId, msgId, callback)-> fs.readFile memoFile(chatId, msgId), 'utf8',
                (err, data)-> callback err, JSON.parse(data or '{}')
writeMemo = (chatId, msgId, data, callback)-> fs.writeFile memoFile(chatId, msgId), JSON.stringify(data), callback
delMemo = (chatId, msgId)-> fs.unlink memoFile(chatId, msgId), -> 0

initGame = (player1, player2, chatId, bot)->
    gameData = {
        players: [null, player1, player2]
        turnOwner: 1
        board: [[0,0,0],[0,0,0],[0,0,0]]
    }
    bot.sendMessage mkGameText(gameData), chatId,
    parse_mode: 'Markdown',
    reply_markup: mkBoard(gameData),
    (err, response, result)=>
        return bot.logErrorAndMsgAdm err, 'Cant init Tic Tac Toe game.' if err
        msgId = result.message_id
        writeMemo chatId, msgId, gameData, (err)-> bot.logErrorAndMsgAdm err if err

mkGameText = (gameData)-> """
    **Jogo da Velha**
    ⭕@#{gameData.players[1]} **Vs** ❌@#{gameData.players[2]}
    É a vez de #{gameData.players[gameData.turnOwner]}.
    """

mkBoard = (gameData)->
    JSON.stringify inline_keyboard: ( for line,y in gameData.board
        ( for place,x in line
            mark = if place is 0 then ' ' else if place is 1 then '⭕' else '❌'
            cmd = if place is 0 then "#{y}x#{x}" else ''
            { text: mark, callback_data: "tic-tac-toe:#{cmd}" }
        )
    )

playerAction = (bot, updateAct, y, x)->
    chatId = updateAct.chat?.id or 0
    msgId = updateAct.message.message_id or 0
    usr = updateAct.from.username
    readMemo chatId, msgId, (err, gameData)->
        if err
            bot.logErrorAndMsgAdm err, 'Fail to load tic-tac-toe momory file.'
            bot.answerCallbackQuery updateAct.id, "Can't load memory file for this game.", true
            bot.editMessageText "Can't load memory file for this game.", chatId, msgId
        else
            if usr is gameData.players[gameData.turnOwner]
                bot.answerCallbackQuery updateAct.id, "Jogada de #{usr}.", false
                gameData.board[y][x] = gameData.turnOwner
                gameData.turnOwner = if gameData.turnOwner is 1 then 2 else 1
                rebuildBoard gameData, bot, chatId, msgId
            else
                turnOwner = gameData.players[gameData.turnOwner]
                bot.answerCallbackQuery updateAct.id, "Ops... Essa é a vez de #{turnOwner}.", true

rebuildBoard = (gameData, bot, chatId, msgId)->
    writeMemo chatId, msgId, gameData, (err)-> bot.logErrorAndMsgAdm err if err
    b = gameData.board
    p = (y,x)-> if b[y][x] is 0 then '⬜' else if b[y][x] is 1 then '⭕' else '❌'
    if whoWon = testVictory gameData
        bot.editMessageText """
        **Jogo da Velha**
        ⭕@#{gameData.players[1]} **Vs** ❌@#{gameData.players[2]}
        Vitória de #{gameData.players[whoWon]}!
        #{p 0,0}#{p 0,1}#{p 0,2}
        #{p 1,0}#{p 1,1}#{p 1,2}
        #{p 2,0}#{p 2,1}#{p 2,2}
        """, chatId, msgId, parse_mode: 'Markdown'
        delMemo chatId, msgId
    else
        if b.join().indexOf('0') > -1
            # There is some empty space.
            bot.editMessageText mkGameText(gameData), chatId, msgId,
                parse_mode: 'Markdown', reply_markup: mkBoard(gameData)
        else
            bot.editMessageText """
            **Jogo da Velha**
            ⭕@#{gameData.players[1]} **Vs** ❌@#{gameData.players[2]}
            Deu velha.
            #{p 0,0}#{p 0,1}#{p 0,2}
            #{p 1,0}#{p 1,1}#{p 1,2}
            #{p 2,0}#{p 2,1}#{p 2,2}
            """, chatId, msgId, parse_mode: 'Markdown'
            delMemo chatId, msgId

testVictory = (gameData)->
    board = gameData.board
    # Test lines
    for y in [0..2]
        return board[y][0] if board[y][0] is board[y][1] is board[y][2]
    # Test columns
    for x in [0..2]
        return board[0][x] if board[0][x] is board[1][x] is board[2][x]
    # Test diagonal 1
    return board[0][0] if board[0][0] is board[1][1] is board[2][2]
    # Test diagonal 2
    return board[0][2] if board[0][2] is board[1][1] is board[2][0]
    return null


